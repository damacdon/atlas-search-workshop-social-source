# ATLAS search workshop social source

This is the source code for the C++ executable used in https://gitlab.cern.ch/damacdon/atlas-search-workshop-social to check the input CI variables and (if the inputs are correct) output instrucions for next steps as an artifact. This is part of the escape room for the Sept. 2021 social event.

# Instructions to compile executable

The executable needs to be compiled in the gcc docker image used to run the CI in the escape room to ensure compatibility. To compile the executable in the docker image, create a docker container from the top level of this gitlab project (containing the source code), volume-mounting the current working directory:

```bash
docker run --rm -it -v $PWD:/workdir -w /workdir gcc:9.4 /bin/bash
```

Then inside the container, compile the source code to create the executable:

```bash
g++ check_code.cxx -o check_code
```
 You can now copy the updated executable `check_code` to `bin` directory in https://gitlab.cern.ch/damacdon/atlas-search-workshop-social.
