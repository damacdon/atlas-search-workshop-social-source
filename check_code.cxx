/*
 C++ code to be used in gitlab CI for escape room in Sept. 2021 ATLAS search social event.
 */

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <string>

int main(int argc, char **argv)
{
  // Get the input passcode
  std::string input_code = argv[1];
  
  // Convert the input passcode to lower case
  std::transform(input_code.begin(), input_code.end(), input_code.begin(),
  [](unsigned char c){ return std::tolower(c); });
  
  // Define correct code, acceptable group names, and associated output codes
  std::string correct_code = "quark";
  std::string output_code = "kraqueDsm";
  std::ofstream f_output("output_message.txt");
  
  // If the group is found, check if the input code is correct
  if(input_code.find(correct_code) != std::string::npos) {
    
    // Yay, the input code is correct -- write the output code to a text file
    f_output << "Congratulations, you are one step closer to escaping the Standard Model! Go back to your google form and enter the unique passphrase '" << output_code << "' to continue your journey!";
  }
  
  // Wrong input code
  else {
    f_output << "We're terribly sorry, but your guess of '" << input_code << "' does not match our records. Please hang up, put in a new guess, and try your commit again.\n";
  }
  f_output.close();
    
  return 0;
}
